/**
 * @file interpreter.h
 * @brief Header file for the interpreter.
 * @details This file contains the interpreter for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include "../utils/typedef.h"
#include "../utils/utils.h"
#include "../builtin/builtin.h"
#include "../utils/history.h"

#include <sys/types.h>
#include <sys/wait.h>

/**
 * @brief Execute if a command is an alias
 *
 * This function, `isAliasExecute`, checks if it is an alias.
 * Then, the function executes the alias.
 * The function finally returns the exit status of the command.
 *
 * @param command The command args
 *
 * @return exit status of the command
 */
int isAliasExecute(Command command);
/**
 * @brief Execute if a command is a built-in command
 *
 * This function, `isBuiltInCommandExecute`, checks if it is a built-in command.
 * Then, the function executes the built-in command.
 * The function finally returns the exit status of the command.
 *
 * @param command The command args
 *
 * @return exit status of the command
 */
int isBuiltInCommandExecute(Command command);

/**
 * @brief Execute if a command is a real command
 *
 * This function, `isCommandExecute`, checks if it is a real command from bash.
 * Then, the function executes the real command.
 * The function finally returns the exit status of the command.
 *
 * @param command The command args
 *
 * @return exit status of the command
 */
int isCommandExecute(Command command);

/**
 * @brief Execute if a command if it exists
 *
 * This function, `executeCommandIfExists`, checks if the command exists as built-in, or as a real command.
 * Then, the function executes the command.
 * The function finally returns the exit status of the command.
 *
 * @param command The command args
 *
 * @return exit status of the command
 */
int executeCommandIfExists(Command command);
#endif /* INTERPRETER_H_ */