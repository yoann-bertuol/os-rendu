# Projet Minishell

## Sommaire

1. [Introduction](#introduction)
1. [Fonctionnalités](#fonctionnalités)
1. [Contraintes Techniques](#contraintes-techniques)
1. [Contraintes Techniques Optionnelles](#contraintes-techniques-optionnelles)
1. [Architecture](#architecture)
1. [Installation](#installation)
1. [Utilisation](#utilisation)
1. [Auteurs](#auteurs)

## Introduction

Ce projet a pour but de recoder le shell bash, dans une version très simplifiée.

Au lancement du programme, le shell affiche un prompt et attend que
l'utilisateur entre une commande. Une fois la commande entrée, le shell
l'exécute, affiche le résultat de la commande et affiche à nouveau le prompt.

## Fonctionnalités

Dans le sujet, plusieurs fonctionnalités sont demandées. Nous avons :

### FM01 : Gestion des commandes

Les commandes utilisables sont toutes les commandes du système, en vérifiant
grâce à la fonction `system(which <commande>)` si la commande existe. Nous
pouvons alors l'éxécuter grâce à la méthode `system(<commande>)`.

### FM02 : Gestion des sous ensembles de commandes

Non impléménté.

### FM03 : Gestion des commandes "built-in"

Les commandes built-in recréées sont :

-   `cd` : permet de changer de répertoire
-   `pwd` : permet d'afficher le répertoire courant
-   `echo` : permet d'afficher un message
-   `exit` : permet de quitter le shell
-   `setenv` : permet de définir une variable d'environnement
-   `unsetenv` : permet de supprimer une variable d'environnement
-   `alias` : permet de définir un alias (cf. [FM07](#fm07-gestion-des-alias))
-   `unalias` : permet de supprimer un alias (cf.
    [FM07](#fm07-gestion-des-alias))

### FM04 : Gestion d'un historique

L'historique nous permets de lister les commandes précédemment entrées, avec
leur date et heure au format ISO 8601. ce fichier se trouve de manière statique
dans le système de fichier `/tmp/.minishell_history`.

Un fichier statique est utilisé pour stocker l'historique, afin de ne pas avoir
de soucis d'écriture par rapport au répertoire courant. Nous aurions voulu le
mettre dans le dossier home de l'utilisateur, mais nous n'avons pas réussi à le
faire.

Un exemple de ce qu'il est possible d'obtenir :

```bash
[2023-02-07T00:34:39] ls
[2023-02-07T00:34:39] cd
[2023-02-07T00:37:18] cd ..
[2023-02-07T00:37:21] pwd
[2023-02-07T00:37:30] echo "Hello World"
```

Seule la commande `exit` n'est pas enregistrée dans l'historique.

### FM05 : Gestion d'un mode batch (à execution immédiate)

Le mode batch permet d'exécuter le programme directement avec des paramètres, en
ayant une seule éxécution du programme. Il est possible de l'utiliser de la
manière suivante :

```bash
./minishell <commande>
```

### FM06 : Gestion des variables d'environnement

Non impléménté. Les fonctionnalités de `setenv` et `unsetenv` sont implémentées
mais ne répondent pas aux exigences du sujet initial.

### FM07 : Gestion des alias

Les alias sont des raccourcis pour des commandes. Ils sont stockés dans un
tableau d'alias, et sont utilisables dans le shell. Ils sont définis de la
manière suivante :

```bash
alias <alias>="<commande>"
```

Il sera alors vérifié si la commande précisé est un alias avant de résoudre le
réel nom de la commande.

Il est aussi possible de supprimer un alias avec la commande `unalias`.

```bash
unalias <alias>
```

## Contraintes Techniques

Dans le sujet, plusieurs contraintes techniques sont demandées. Nous avons :

### CT01 : Compilation via Makefile

Le projet est compilé via un Makefile. Il est documenté et permet de compiler et
de générer une documentation

### CT02 : Fichier typedef.h

Le fichier `typedef.h` contient les structures et les prototypes des fonctions.
Il se trouve dans le dossier `src/utils/`.

### CT03 : Définitions et implémentations des fonctions dans des fichiers séparés

Les fonctions sont définies dans des fichiers `.h`, et sont implémentées dans
des fichiers `.c`. Ils se trouvent en binôme dans leurs dossiers respectifs.

### CT04 : Documentation

La documentation est présente dans les fichier `.h`. Elle est aussi générée avec
Doxygen (cf [CTO01](#cto01--génération-de-la-documentation)).

### CT05 : Gestion des erreurs

Les erreurs sont gérées avec les mécanismes proposés par `errno`. La seule
exception se fait sur l'affichage d'une commande inexistante car errno n'a pas
le message d'erreur correspondant.

## Contraintes Techniques Optionnelles

Dans le sujet, plusieurs contraintes techniques optionnelles sont proposées.
Nous avons :

### CTO01 : Génération de la documentation

La documentation est générée avec Doxygen et son processus de création est
documenté dans le Makefile ainsi que dans le fichier Doxyfile. Elle est
disponible dans le dossier `doc/`.

## Architecture

L'architecture du projet est la suivante :

```bash
.
Makefile
Doxyfile
subject.pdf
README.md
├── bin
│   ├── minishell => executable
├── doc
│   ├── html
│   ├── latex
├── src
│   ├── builtin => commandes built-in
│   ├── interpreter => interpréteur de commandes
│   ├── runner => exécution des modes (batch, interactive)
│   ├── utils => fonctions utilitaires
│   ├── main.c => point d'entrée du programme
```

## Installation

Pour installer le projet, il suffit de cloner le dépôt git :

```bash
git clone https://gitlab.com/yoann-bertuol/projet-os-c.git
cd projet-os-c
```

Il est aussi nécéssaire de le compiler :

```bash
make
```

L'éxécutable se trouve dans le dossier `bin/`.

## Utilisation

Pour utiliser le projet, il suffit de lancer l'éxécutable :

```bash
./bin/minishell
```

Il est aussi possible de lancer le programme en mode batch :

```bash
./bin/minishell <commande>
```

## Auteurs

-   Yoann BERTUOL
-   Yasmine BOUMAZZOUGH

