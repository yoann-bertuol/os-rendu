/**
 * @file main.c
 * @brief Main file for the program.
 * @details This file contains the main function for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#include "runner/runner.h"

/**
 * @fn int main(int argc, char const **argv)
 * @brief Main function for the program.
 * @details This function is the main function for the program.
 * @param argc The number of arguments.
 * @param argv The arguments.
 * @return The exit code.
 */
int main(int argc, char const **argv)
{
	if (argc > 1)
	{
		return batchRun(argc, argv);
	}
	return defaultRun(argc, argv);
}
