#include "interpreter.h"

int isAliasExecute(Command command)
{
	// verify if command argv 0 is in environnement variable
	Command *alias = getAlias(command.argv[0]);
	if (alias != NULL)
	{
		executeCommandIfExists(*alias);
		return EXIT_SUCCESS;
	}
	else
	{
		return EXIT_FAILURE;
	}
}

int isBuiltInCommandExecute(Command command)
{
	if (!strcmp(command.argv[0], "exit"))
	{
		shellExit();
	}
	else if (!strcmp(command.argv[0], "cd"))
	{
		shellCd(command);
	}
	else if (!strcmp(command.argv[0], "echo"))
	{
		shellEcho(command);
	}
	else if (!strcmp(command.argv[0], "pwd"))
	{
		shellPwd();
	}
	else if (!strcmp(command.argv[0], "alias"))
	{
		shellAlias(command);
	}
	else if (!strcmp(command.argv[0], "unalias"))
	{
		shellUnalias(command);
	}
	else if (!strcmp(command.argv[0], "set"))
	{
		shellSet(command);
	}
	else if (!strcmp(command.argv[0], "unset"))
	{
		shellUnset(command);
	}
	else
	{
		return EXIT_FAILURE;
	}
	writeCommandInHistory(command);
	return EXIT_SUCCESS;
}

int isCommandExecute(Command command)
{
	char *commandBase = calloc(NB_MAX_CHAR_IN_STRING, sizeof(char));
	char *joinedCommand = calloc(NB_MAX_CHAR_IN_STRING, sizeof(char));
	sprintf(commandBase, "which %s > /dev/null 2>&1", command.argv[0]);
	if (system(commandBase))
	{
		perror("which");
		safelyFreeMemory(commandBase);
		safelyFreeMemory(joinedCommand);
		return EXIT_FAILURE;
	}
	safelyFreeMemory(commandBase);

	pid_t pid = fork();
	if (pid == 0)
	{
		if (joinCommandIntoBuffer(joinedCommand, " ", command))
		{
			errno = EINVAL;
			perror("joinCommandIntoBuffer");
			exit(EXIT_FAILURE);
		}
		if (system(joinedCommand))
		{
			errno = EINVAL;
			perror("system");
			exit(EXIT_FAILURE);
		}
		writeCommandInHistory(command);
		exit(EXIT_SUCCESS);
	}
	else
	{
		int status;
		waitpid(pid, &status, 0);
		safelyFreeMemory(joinedCommand);
		return status;
	}
}

int executeCommandIfExists(Command command)
{
	if (!isAliasExecute(command))
	{
		return EXIT_SUCCESS;
	}
	if (!isBuiltInCommandExecute(command))
	{
		return EXIT_SUCCESS;
	}
	if (!isCommandExecute(command))
	{
		return EXIT_SUCCESS;
	}
	printf("%s: command not found\n", command.argv[0]);
	return EXIT_FAILURE;
}