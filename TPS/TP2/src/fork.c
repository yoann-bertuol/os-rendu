#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
    int f;
    int status;
    f = fork();
    if (f==0){
        int pid = getpid();
        int ppid = getppid();
        printf("Child pid: %d\n", pid);
        printf("Parent pid : %d\n", ppid);

        int exitCode=pid %10;
        exit(exitCode);
    }
    else{
        int pid = getpid();
        printf("Parent pid : %d\n", pid);
        wait(&status);
        int exitCode= WEXITSTATUS(status);
        printf("Return code %d\n",exitCode);
    }
}