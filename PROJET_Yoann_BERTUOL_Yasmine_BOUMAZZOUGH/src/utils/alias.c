#include "alias.h"

Alias **aliasList = NULL;
int aliasListSize;

int initAliasList()
{
	aliasList = calloc(NB_MAX_STRING, sizeof(Alias *));
	aliasListSize = 0;
	return EXIT_SUCCESS;
}

int freeAliasList()
{
	for (int i = 0; i < aliasListSize; i++)
	{
		safelyFreeMemory(aliasList[i]->alias);
		safelyFreeMemory(aliasList[i]->command);
		safelyFreeMemory(aliasList[i]);
	}
	safelyFreeMemory(aliasList);
	return EXIT_SUCCESS;
}

int addAlias(char *aliasName, char *aliasValue)
{
	Alias *alias = (Alias *)malloc(sizeof(Alias));
	alias->command = (Command *)malloc(sizeof(Command));
	alias->alias = strdup(aliasName);
	alias->command->argc = 1;
	alias->command->argv = (char **)malloc(sizeof(char *));
	alias->command->argv[0] = strdup(aliasValue);
	aliasList[aliasListSize] = alias;
	aliasListSize++;

	return EXIT_SUCCESS;
}

int removeAlias(char *aliasName)
{
	for (int i = 0; i < aliasListSize; i++)
	{
		if (strcmp(aliasList[i]->alias, aliasName) == 0)
		{
			safelyFreeMemory(aliasList[i]->alias);
			safelyFreeMemory(aliasList[i]->command);
			safelyFreeMemory(aliasList[i]);
			aliasList[i] = aliasList[aliasListSize - 1];
			aliasListSize--;
			return EXIT_SUCCESS;
		}
	}
	return EXIT_FAILURE;
}

Command *getAlias(char *aliasName)
{
	for (int i = 0; i < aliasListSize; i++)
	{
		if (strcmp(aliasList[i]->alias, aliasName) == 0)
		{
			return aliasList[i]->command;
		}
	}
	return NULL;
}