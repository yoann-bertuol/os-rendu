/**
 * @file runner.h
 * @brief Header file for the runner.
 * @details This file contains the runner for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef RUNNER_H
#define RUNNER_H

#include "../interpreter/interpreter.h"
#include "../utils/typedef.h"
#include "../utils/utils.h"
#include "../utils/alias.h"

#include <stdio.h>
#include <stdlib.h>

/**
 * @brief      Run the shell in batch mode
 * @details    Run a specific command in the shell, then exit
 * @param      argc  The argc
 * @param      argv  The argv
 *
 * @return     EXIT_SUCCESS if the command has been executed
 */
int batchRun(int argc, char const **argv);

/**
 * @brief      Run the shell in default mode
 * @details    Run the shell in default mode, waiting for a command
 * @param      argc  The argc
 * @param      argv  The argv
 *
 * @return     EXIT_SUCCESS if the command has been executed
 */
int defaultRun(int argc, char const **argv);

#endif /* RUNNER_H */