/**
 * @file alias.h
 * @brief Alias management
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef ALIAS_H
#define ALIAS_H

#include "typedef.h"
#include "utils.h"

/**
 * @brief Initialize the list of aliases
 * @return EXIT_SUCCESS if the list was initialized successfully, EXIT_FAILURE otherwise
 */
int initAliasList();

/**
 * @brief Get the alias value from the list of aliases
 * @param aliasName The name of the alias
 * @return The value of the alias if it exists, NULL otherwise
 */
Command *getAlias(char *aliasName);

/**
 * @brief Add an alias to the list of aliases
 * @param aliasName The name of the alias
 * @param aliasValue The value of the alias
 * @return EXIT_SUCCESS if the alias was added successfully, EXIT_FAILURE otherwise
 */
int addAlias(char *aliasName, char *aliasValue);

/**
 * @brief Remove an alias from the list of aliases
 * @param aliasName The name of the alias
 * @param aliasValue The value of the alias
 * @return EXIT_SUCCESS if the alias was removed successfully, EXIT_FAILURE otherwise
 */
int removeAlias(char *aliasName);

/**
 * @brief free the alias list
 * @return EXIT_SUCCESS if the list was freed successfully, EXIT_FAILURE otherwise
 */
int freeAliasList();

#endif /* ALIAS_H */