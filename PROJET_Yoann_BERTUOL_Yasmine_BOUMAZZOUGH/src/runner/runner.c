#include "runner.h"

int batchRun(int argc, char const **argv)
{
	char *bufferCommand = calloc(NB_MAX_CHAR_IN_STRING, sizeof(char));
	Command *command = calloc(1, sizeof(Command));
	command->argv = calloc(NB_MAX_STRING, sizeof(char *));
	for (int i = 1; i < argc; i++)
	{
		sprintf(bufferCommand, "%s ", argv[i]);
	}
	splitBufferIntoCommand(bufferCommand, " ", command);
	executeCommandIfExists(*command);
	safelyFreeMemory(command->argv);
	safelyFreeMemory(command);
	safelyFreeMemory(bufferCommand);
	return EXIT_SUCCESS;
}

int defaultRun(int argc, char const **argv)
{
	initAliasList();
	char *bufferCommand = NULL;
	size_t bufferSize = 0;
	size_t commandSize = 0;

	while (TRUE)
	{
		printf("$> ");
		commandSize = getline(&bufferCommand, &bufferSize, stdin);
		if (commandSize == 1)
		{
			continue;
		}
		Command *command = calloc(1, sizeof(Command));
		command->argv = calloc(NB_MAX_STRING, sizeof(char *));
		splitBufferIntoCommand(bufferCommand, " ", command);
		executeCommandIfExists(*command);
		safelyFreeMemory(command->argv);
		safelyFreeMemory(command);
	}
	freeAliasList();
	return EXIT_SUCCESS;
}