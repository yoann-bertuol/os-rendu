#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define STDOUT 1
#define STDERR 2

int main(int argc, char** argv){
    int f;
    f = fork();
    if(f==0)
    {
        printf("Child PID : %d\n", getpid());
        close(STDERR);
        static char template[] = "/tmp/proc-exercice";
        char filename[25];
        strcpy(filename, template);
        int fd = mkstemp(filename);
        dprintf(STDOUT, "Child file descriptor %s is : %d \n", filename, fd);
        execlp (argv[1], argv[1], (void*)0);
		dprintf(STDOUT, "Message quelconque !\n");
        perror("Error : ");
        exit(1);
    }
    else
    {
        int retour = 0;
        printf("Parent PID : %d\n", getpid());
        wait(&retour);
        dprintf(STDERR, "Message quelconque !\n");
        dprintf(STDOUT, "That's All Folks !\n");
    }
    return EXIT_SUCCESS;
}
