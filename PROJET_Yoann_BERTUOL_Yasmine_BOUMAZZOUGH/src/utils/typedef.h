/**
 * @file typedef.h
 * @brief Header file for the typedefs.
 * @details This file contains Type definitions for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef TYPEDEF_H_
#define TYPEDEF_H_

#define TRUE 1
#define FALSE 0
#define NB_MAX_STRING 10
#define NB_MAX_CHAR_IN_STRING 100

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Generate doxygen documentation for this struct
/**
 * @struct Command
 * @brief Struct containing the input split by space.
 * @var Command::argc
 * Member 'argc' is the length of the array.
 * @var Command::argv
 * Member 'argv' is an array of strings containing the input split by space.
 */
typedef struct command
{
	int argc;
	char **argv;
} Command;

/**
 * @struct Alias
 * @brief Struct containing the alias and the command.
 * @var Alias::alias
 * Member 'alias' is the alias.
 * @var Alias::command
 * Member 'command' is the command.
 */
typedef struct alias
{
	char *alias;
	Command *command;
} Alias;

#endif /* TYPEDEF_H_ */