/**
 * @file history.h
 * @brief Header file for the history.
 * @details This file contains the history for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef HISTORY_H
#define HISTORY_H

#include "typedef.h"

#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#define HISTORY_FILE_PATH "/tmp/.minishell_history"

/**
 * @brief Write a command in the history file
 * @param command The command to write
 * @return 0 if the command has been written, -1 otherwise
 */
int writeCommandInHistory(Command command);

#endif