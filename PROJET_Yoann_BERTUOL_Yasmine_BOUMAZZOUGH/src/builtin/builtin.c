#include "builtin.h"

void removeCharacter(char *string, char character)
{
	char *readPointer = string;
	char *writePointer = string;
	while (*readPointer)
	{
		*writePointer = *readPointer++;
		writePointer += (*writePointer != character);
	}
	*writePointer = '\0';
}

void shellExit()
{
	exit(EXIT_SUCCESS);
}

int shellCd(Command command)
{
	switch (command.argc)
	{
	case 1:
		chdir(getenv("HOME"));
		break;
	case 2:
		if (chdir(command.argv[1]) == -1)
		{
			perror("cd");
		}
		return EXIT_FAILURE;

	default:
		errno = EINVAL;
		perror("cd");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int shellEcho(Command command)
{
	for (int i = 1; i < command.argc; i++)
	{
		printf("%s ", command.argv[i]);
	}
	printf("\n");
	return EXIT_SUCCESS;
}

int shellPwd()
{
	char *pwd = getcwd(NULL, 0);
	printf("%s\n", pwd);
	safelyFreeMemory(pwd);
	return EXIT_SUCCESS;
}

int shellAlias(Command command)
{
	char *alias = command.argv[1];
	char *aliasName = strtok(alias, "=");
	char *aliasValue = strtok(NULL, "=");
	switch (command.argc)
	{
	case 2:
		if (aliasValue == NULL)
		{
			errno = ENOENT;
			perror("alias");
			return EXIT_FAILURE;
		}
		removeCharacter(aliasValue, '"');
		addAlias(aliasName, aliasValue);
		break;
	case 1:
	default:
		errno = EINVAL;
		perror("alias");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int shellUnalias(Command command)
{

	char *aliasName = command.argv[1];
	switch (command.argc)
	{
	case 2:
		if (removeAlias(aliasName))
		{
			errno = ENOENT;
			perror("unalias");
			return EXIT_FAILURE;
		}
		break;
	case 1:
	default:
		errno = EINVAL;
		perror("unalias");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int shellSet(Command command)
{
	char **envp = environ;
	char *value = getenv(command.argv[1]);
	switch (command.argc)
	{
	case 1:
		while (*envp != NULL)
		{
			printf("%s\n", *envp++);
		}
		break;
	case 2:
		if (value == NULL)
		{
			errno = ENOENT;
			perror("set");
			return EXIT_FAILURE;
		}
		printf("%s=%s\n", command.argv[1], value);
		break;

	default:
		if (setenv(command.argv[1], command.argv[2], 1))
		{
			perror("setenv");
			return EXIT_FAILURE;
		}
		break;
	}
	return EXIT_SUCCESS;
}

int shellUnset(Command command)
{
	switch (command.argc)
	{
	case 2:
		if (unsetenv(command.argv[1]) != 0)
		{
			perror("unsetenv");
		}
		break;
	case 1:
	default:
		errno = EINVAL;
		perror("unset");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}