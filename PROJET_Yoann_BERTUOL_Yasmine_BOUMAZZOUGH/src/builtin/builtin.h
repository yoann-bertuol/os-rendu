/**
 * @file builtin.h
 * @brief Header file for the built-in commands.
 * @details This file contains the built-in commands for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef BUILTIN_H_
#define BUILTIN_H_

#include "../utils/typedef.h"
#include "../utils/utils.h"
#include "../utils/alias.h"

#include <errno.h>
#include <unistd.h>
#include <signal.h>

extern char **environ;

/**
 * @brief      Remove a character from a string
 * @param      string   The string
 * @param      character  The character
 */
void removeCharacter(char *string, char character);

/**
 * @brief      Exit the shell with status EXIT_SUCCESS
 */
void shellExit();

/**
 * @brief      Change the current working directory
 * @param[in]  command  The command
 * @return     EXIT_SUCCESS if success, EXIT_FAILURE if error
 */
int shellCd(Command command);

/**
 * @brief      Echo the arguments
 * @param[in]  argc  arguments count
 * @param      argv  arguments
 * @return     EXIT_SUCCESS if success
 */
int shellEcho(Command command);

/**
 * @brief      Print the current working directory
 * @return     EXIT_SUCCESS if success
 */
int shellPwd();

/**
 * @brief      Add an alias
 * @return     EXIT_SUCCESS if success, EXIT_FAILURE if error
 */
int shellAlias(Command command);

/**
 * @brief      Remove an alias
 * @return     EXIT_SUCCESS if success, EXIT_FAILURE if error
 */
int shellUnalias(Command command);

/**
 * @brief      Print the list of aliases
 * @return     EXIT_SUCCESS if success, EXIT_FAILURE if error
 */
int shellSet(Command command);

/**
 * @brief      Print the list of aliases
 * @return     EXIT_SUCCESS if success, EXIT_FAILURE if error
 */
int shellUnset(Command command);

#endif /* BUILTIN_H_ */