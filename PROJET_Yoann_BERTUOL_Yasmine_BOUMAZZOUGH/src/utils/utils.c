#include "utils.h"

void safelyFreeMemory(void *memoryToFree)
{
	if (memoryToFree != NULL)
	{
		free(memoryToFree);
		memoryToFree = NULL;
	}
}

int splitBufferIntoCommand(char *buffer, char *delimiter, Command *command)
{
	int numberOfParts = 0;

	// Remove the \n character from the buffer
	buffer[strcspn(buffer, "\n")] = 0;
	char *token;

	// Split the buffer into parts using the delimiter
	token = strtok(buffer, delimiter);

	// Store the parts in the array
	while (token != NULL)
	{
		command->argv[numberOfParts++] = token;
		token = strtok(NULL, delimiter);
	}

	// Set the last element of the array to NULL
	command->argc = numberOfParts;
	command->argv[numberOfParts++] = NULL;

	return EXIT_SUCCESS;
}

int joinCommandIntoBuffer(char *buffer, char *delimiter, Command command)
{
	// Join the parts of the array into a buffer using the delimiter
	for (size_t i = 0; i < command.argc; i++)
	{
		strcat(buffer, command.argv[i]);
		strcat(buffer, delimiter);
	}
	// Log buffer
	// Remove the last delimiter from the buffer
	buffer[strlen(buffer) - 1] = 0;

	return EXIT_SUCCESS;
}
