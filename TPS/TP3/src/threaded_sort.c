#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>

#define SIZE (int)1e8

int tab[SIZE];
int nbThread[5] = {0, 2, 4, 6, 8};
int minValInArray;
pthread_mutex_t lockMin;
int maxValInArray;
pthread_mutex_t lockMax;

typedef struct threadStruct {
	int* array;
	int size;
} ThreadStruct;

void initTab() {
	int i;
	srand(time(NULL));
	for (i = 0; i < SIZE; i++) {
		tab[i] = rand();
	}
	minValInArray = tab[0];
	pthread_mutex_init(&lockMin, NULL);
	maxValInArray = tab[0];
	pthread_mutex_init(&lockMax, NULL);
};

void *min(void* arguments) {
	ThreadStruct* threadStruct = (ThreadStruct *) arguments;
	int i;
	for(i = 1; i < threadStruct->size; i++) {
		if (minValInArray > threadStruct->array[i])
		{
			pthread_mutex_lock(&lockMin);
			minValInArray = threadStruct->array[i];
			pthread_mutex_unlock(&lockMin);
		}
	}
}

void *max(void* arguments) {
	ThreadStruct* threadStruct = (ThreadStruct *) arguments;
	int i;
	for (i = 1; i < threadStruct->size; i++)
	{
		if(maxValInArray < threadStruct->array[i]) {
			pthread_mutex_lock(&lockMax);
			maxValInArray = threadStruct->array[i];
			pthread_mutex_unlock(&lockMax);
		}
	}
}

void createThread(int nbThread, void* arguments, void  *(*f)(void *)) {
	pthread_t tabThread[nbThread];

	if(nbThread <= 0) {
		f(arguments);
		return 0;
	}
	ThreadStruct* threadStruct = malloc(sizeof(ThreadStruct));
	threadStruct->size = ((ThreadStruct *) arguments)->size / nbThread;
	threadStruct->array = ((ThreadStruct *) arguments)->array;
	int i = 0, modulo = ((ThreadStruct *) arguments)->size % nbThread;
	if(modulo != 0) {
		pthread_create(&tabThread[i], NULL, f,threadStruct);
		i = 1;
	}
	for (; i < nbThread; i++)
	{
		threadStruct->array = ((ThreadStruct *) arguments)->array + (i * threadStruct->size + modulo);
		pthread_create(&tabThread[i], NULL, f,threadStruct);
	}
	for (i = 0; i < nbThread; i++)
	{
		pthread_join(tabThread[i], NULL);
	}
	return 0;

}

int main(int argc, char** argv) {
	initTab();

	printf("Length : %d \n", SIZE);
	ThreadStruct* threadStruct = malloc(sizeof(ThreadStruct));
	threadStruct->array = tab;
	threadStruct->size = SIZE;
	struct timeval before, after;

	for (int i = 0; i < 5; i++)
	{
		printf("%d thread : \n", nbThread[i]);
		gettimeofday(&before, NULL);
		createThread(nbThread[i], threadStruct, *min);
		gettimeofday(&after, NULL);
		printf("-- Min = %d \n", minValInArray);
		printf("---- Finding Time : %ld us\n", after.tv_usec - before.tv_usec);
		gettimeofday(&before, NULL);
		createThread(nbThread[i], threadStruct, *max);
		gettimeofday(&after, NULL);
		printf("-- Max = %d \n", maxValInArray);
		printf("---- Finding Time : %ld us\n", after.tv_usec - before.tv_usec);
	}
	pthread_mutex_destroy(&lockMin);
	pthread_mutex_destroy(&lockMax);
	return EXIT_SUCCESS;
};