#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#include <sys/fcntl.h>


/**
 * L'objectif de ce code est de mettre en place d'un processus :
 * ===
 * - générant deux processus fils de même niveau hiérarchique.
 * - réalisant une communication ascendante fils -> père via les tubes.
 * ===
 * Schématiquement :
 * ===
 *       Père
 *       ^ ^
 *       | |
 * Fils1 | | Fils2
 */

int main(int argc, char** argv) {
    int pipe_1[2];
    int statval1, statval2;

    /**
     * La création du tube à ce stade permet de rendre disponible
     * le medium de communication au sein de n'importe quel fils
     * (généré par un appel fork())
     */
    if (pipe(pipe_1) < 0) {
        perror("Unable to open a pipe!");
        exit(1);
    }

    // Création du FILS N°1
    if (fork() == 0) {
        /**
         * Fermeture des descripteurs non utilisés :
         * communication ascendante fils->père = fermeture du descripteur de lecture (indice 0)
         */
        close(pipe_1[0]);
        dup2(pipe_1[1], 1);

        // Ecriture de données dans le tube.
        write(pipe_1[1], "CHILD1", sizeof(char)*6);
        close(pipe_1[1]);
        execlp ("ps", "ps", "eaux", (void*)0);
        // Affichage du PID du processus
        printf("[CHILD1] pid :%d\n", getpid());
        // Terminaison du nominal processus via code retour = 0;
        perror("ps error : ");
        exit(1);
    }

    // Création du FILS N°2 via fork()
    if (fork() == 0) {
        // Code réalisé par le père (valeur retour de fork() != 0)
        close(pipe_1[1]);

        int i = 0;
        char buffer;
        // Lecture de toute donnée disponible dans le tube.
        while(read(pipe_1[0], &buffer, sizeof(char)) != 0) {
            printf("%c\n", buffer);
        }

        // Attente de terminaison des 2 processus fils pour ne pas générer des processus zombies.
        wait(NULL);
        wait(NULL);

        // Création du fichier dans /dev/null
        int fd = open("/dev/null", O_WRONLY);
        dup2(fd, 1);

        // Execute la commande grep root
        execlp ("grep", "grep", "^root", (void*)0);

        // Récupère les erreurs
        perror("grep error : ");
        exit(1);

    }

    close(pipe_1[1]);
    close(pipe_1[0]);

    // Attend la fin des processus
    wait(&statval1);
    wait(&statval2);

    // Vérifie si les processus se sont terminés normalement
    if(WIFEXITED(statval1) && WIFEXITED(statval2) && WEXITSTATUS(statval1) == 0 && WEXITSTATUS(statval2) == 0 )
        write(1, "root is connected\n", 18);
    return EXIT_SUCCESS;
}