/**
 * \file skeleton.c
 * \brief Basic parsing options skeleton.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \version 0.1
 * \date 10 septembre 2016
 *
 * Basic parsing options skeleton exemple c file.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <getopt.h>
#include <sys/stat.h>
#include <time.h>
#include <pwd.h>
#include <fcntl.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
  -p, --option \n\
***\n\
  -p c : Copying a file\n\
  -p r : Reverse a file\n\
  -p l : ls like\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);

    // Checking if ERRNO is set
    if (str == NULL)
      perror(strerror(errno));
  }

  return str;
}

int copy(char* bin_input_param, char* bin_output_param) {
  dprintf(STDOUT,"Copying %s in %s\n", bin_input_param, bin_output_param);

  // Setup
  FILE* file = NULL;
  FILE* copy_file = NULL;
  char buffer[512];
  int readBytes;

  // Opening both files, input with read command and copy with creating write command
  file = fopen(bin_input_param, "r+");
  copy_file = fopen(bin_output_param, "a+");

  // Write each line if we get one
  if(file != NULL) {
    while((readBytes = fread(buffer, 1, sizeof(buffer)/sizeof(char), file)) != 0) {
      fwrite(buffer, 1, readBytes, copy_file);
    }
  }

  // Close files
  fclose(file);
  fclose(copy_file);
  return EXIT_SUCCESS;
}

int reverse(char* bin_input_param) {
  // Open file
  int input = open(bin_input_param, O_RDONLY, S_IRUSR | S_IRGRP | S_IROTH);

  if(input >= 0) {
    // Get file size
    off_t input_size = lseek(input, 0, SEEK_END);
    // Set pointer to start of file
    lseek(input, 0, SEEK_SET);
    char* buffer = (char*)malloc(input_size);
    // Read file
    read(input, buffer, input_size);
    // Display file by reading the buffer backwards
    for(int i = input_size; i >= 0; i--){
      dprintf(STDOUT,"%c", buffer[i]);
    }
    dprintf(STDOUT,"\n");
  }

  // Close file
  close(input);
  return EXIT_SUCCESS;
}

int lsLike(char* bin_input_param) {
  struct stat statStruct;
  int error = stat(bin_input_param, &statStruct);
  if(error == -1) {
    if(ENOENT == errno) {
      dprintf(STDERR, "Error : The path does not match to a dir nor file\n");
    }
    perror("stat");
    exit(EXIT_FAILURE);
  }
  if(S_ISDIR(statStruct.st_mode)) {
    DIR* directory = opendir(bin_input_param);
    struct dirent *dirStruct;
    struct passwd *pwd;
    while((dirStruct = readdir(directory))) {
      stat((dirStruct->d_name), &statStruct);
      dprintf(STDOUT,"%-30s", dirStruct->d_name);
      dprintf(STDOUT,"  ");
      dprintf(STDOUT, (S_ISDIR(statStruct.st_mode)) ? "d" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IRUSR) ? "r" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IWUSR) ? "w" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IXUSR) ? "x" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IRGRP) ? "r" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IWGRP) ? "w" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IXGRP) ? "x" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IROTH) ? "r" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IWOTH) ? "w" : "-");
      dprintf(STDOUT, (statStruct.st_mode & S_IXOTH) ? "x" : "-");
      dprintf(STDOUT, "  ");
      pwd = getpwuid(statStruct.st_uid);
      dprintf(STDOUT, "%s",pwd->pw_name);
      dprintf(STDOUT, "  ");
      dprintf(STDOUT, "%d",pwd->pw_uid);
      dprintf(STDOUT, " : ");
      dprintf(STDOUT, "%d",pwd->pw_gid);
      dprintf(STDOUT, "  ");

      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];
      time(&statStruct.st_mtime);
      timeinfo = localtime(&statStruct.st_mtime);
      strftime(buffer, 80, "%x - %I:%M%p", timeinfo);
      dprintf(STDOUT, "|%s| ", buffer);
      dprintf(STDOUT, "%ld bytes\n", statStruct.st_size);
    }
  }
  else {
    dprintf(STDERR, "Error : The path does not match to a dir but a file\n");
    perror("stat");
    exit(EXIT_FAILURE);
  }
  return EXIT_SUCCESS;
}

/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output", optional_argument, 0, 'o' },
  { "option", required_argument, 0, 'p'},
  { 0,         0,                 0,  0  }
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */
const char* binary_optstr = "hvi:o:p:";


/**
 * Binary main loop
 *
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;
  char* bin_option_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'p':
        //option param
        if (optarg)
        {
          bin_option_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
        free_if_needed(bin_option_param);
        exit(EXIT_SUCCESS);
      default :
        break;
    }
  }

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if (bin_input_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    free_if_needed(bin_option_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }

  // Printing params
  dprintf(STDOUT, "** PARAMS **\n%-8s: %s\n%-8s: %s\n%-8s: %s\n%-8s: %d\n",
          "input", bin_input_param,
          "output", bin_output_param,
          "option",  bin_option_param,
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point
  char option_char = (char)*bin_option_param;
  int result = 0;

  if (option_char == 'c') {
    result = copy(bin_input_param, bin_output_param);
  } else if (option_char == 'r') {
    result = reverse(bin_input_param);
  } else if (option_char == 'l') {
    result = lsLike(bin_input_param);
  }

  // Freeing allocated data
  free_if_needed(bin_input_param);
  free_if_needed(bin_output_param);
  free_if_needed(bin_option_param);

  return result;
}
