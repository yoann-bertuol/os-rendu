/**
 * @file utils.h
 * @brief Header file for the tools.
 * @details This file contains the tools for the program.
 * @author Yoann BERTUOL & Yasmine BOUMAZZOUGH
 * @date 2023-02-08
 * @version 1.0
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "typedef.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Safely frees memory
 *
 * This function, `safelyFreeMemory`, takes a pointer to a memory location `memoryToFree` and frees it.
 * The function then sets the pointer to `NULL` to avoid any dangling pointers.
 *
 * @param memoryToFree A pointer to the memory location to be freed
 *
 * @return void
 */
void safelyFreeMemory(void *memoryToFree);

/**
 * @brief Splits a buffer into a command
 *
 * This function, `splitBufferIntoCommand`, takes a buffer `buffer`, a delimiter `delimiter` and a command `command`.
 * The function then splits the buffer into a command using the delimiter.
 *
 * @param buffer The buffer to be split
 * @param delimiter The delimiter to be used
 * @param command The command to be filled
 *
 * @return int
 */
int splitBufferIntoCommand(char *buffer, char *delimiter, Command *command);

/**
 * @brief Joins a command into a buffer
 *
 * This function, `joinCommandIntoBuffer`, takes a buffer `buffer`, a delimiter `delimiter` and a command `command`.
 * The function then joins the command into a buffer using the delimiter.
 *
 * @param buffer The buffer to be filled
 * @param delimiter The delimiter to be used
 * @param command The command to be joined
 *
 * @return int
 */
int joinCommandIntoBuffer(char *buffer, char *delimiter, Command command);

#endif /* UTILS_H_ */