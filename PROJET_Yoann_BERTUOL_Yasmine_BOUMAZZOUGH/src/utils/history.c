#include "history.h"

int writeCommandInHistory(Command command)
{
	FILE *file = fopen(HISTORY_FILE_PATH, "a");
	if (file == NULL)
	{
		perror("fopen");
		return EXIT_FAILURE;
	}

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	fprintf(file, "[%d-%02d-%02dT%02d:%02d:%02d]", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	for (int i = 0; i < command.argc; i++)
	{
		fprintf(file, " %s", command.argv[i]);
	}
	fprintf(file, "\n");
	fclose(file);
	return EXIT_SUCCESS;
}